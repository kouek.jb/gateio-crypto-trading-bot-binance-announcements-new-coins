import requests
import logging
import yaml
from load_config import *
from twilio.rest import Client

config = load_config('config.yml')

with open('auth/auth.yml') as file:
    try:
        creds = yaml.load(file, Loader=yaml.FullLoader)
        bot_token = creds['account_token']
        bot_sid = str(creds['account_sid'])
        valid_auth = True
    except KeyError:
        valid_auth = False
        pass

def send_whatsapp(msg):
    client = Client(bot_sid, bot_token)
    from_whatsapp_number = 'whatsapp:+14155238886'
    to_whatsapp_number = 'whatsapp:+33673337503'
    client.messages.create(body=msg,
                           from_=from_whatsapp_number,
                           to=to_whatsapp_number)

if __name__ == '__main__':
    send_whatsapp('batata')
